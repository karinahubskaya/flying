
$('.review-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    cssEase: 'linear',
    asNavFor: '.review-slider-nav'
});

$('.review-slider-nav').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    asNavFor: '.review-slider',
    dots: false,
    cssEase: 'linear',
    infinite: false,
    focusOnSelect: true,
    responsive: [
        {
            breakpoint: 600,
            settings: {
            slidesToShow: 2,
            slidesToScroll: 1
            }
        }
    ]    
});

let activeSection = $('.section.active').attr('data-id');
$('#' + activeSection).addClass('active');

setTimeout(() => {
    $.scrollify({
        section: ".section-scrollify",
        before: function () {
            $('.section-scrollify').removeClass('active')
            current = $.scrollify.current();
            current.addClass('active');
            let activeSection = current.attr('data-id');
            $('.circle-overlap').removeClass('active');
            $('#' + activeSection).addClass('active');
            $('.safety-inner h2').hide();
            $('.safety-inner p').hide();
            $('.safety-inner img').hide();
            $('.safety-inner.active h2').fadeIn(1800);
            $('.safety-inner.active p').fadeIn(1800);
            $('.safety-inner.active img').fadeIn(2000);
            if (($('.banner.active').length > 0) || ($('.flying-cheap.active').length > 0) || ($('.join-us.active').length > 0) || ($('.discover-flight.active').length > 0) || ($('.make-money.active').length > 0) || ($('.why-rent.active').length > 0)) {
                $('.light-theme').addClass('dark-section-active');
            }
            else {
                $('.light-theme').removeClass('dark-section-active');
            }
            if ($(window).width() > 1279) {
                if ($('.banner.active').length > 0) {
                    $('.nav-switcher').hide();
                }
                else {
                    $('.nav-switcher').show();
                }

                if (($('.banner.active').length > 0) || ($('.flying-cheap.active').length > 0) || ($('.join-us.active').length > 0) || ($('.discover-flight.active').length > 0) || ($('.make-money.active').length > 0) || ($('.why-rent.active').length > 0) || ($('.contact.active').length > 0)) {
                    $('body').addClass('switcher-big-hide');
                }
                else {
                    $('body').removeClass('switcher-big-hide');
                }
            }
        }
    });
}, 1000);

$('.switcher').on('click', function () {
    $('.switch-dark').toggle();
    $('.switch-light').toggle();
    setTimeout(function () {
        $('body').toggleClass('dark-theme');
        $('body').toggleClass('light-theme');
        if (($('.banner.active').length > 0) || ($('.flying-cheap.active').length > 0) || ($('.join-us.active').length > 0) || ($('.discover-flight.active').length > 0) || ($('.make-money.active').length > 0) || ($('.why-rent.active').length > 0) || ($('.contact.active').length > 0)) {
            $('.light-theme').addClass('dark-section-active');
        }
        else {
            $('.light-theme').removeClass('dark-section-active');
            $('body').removeClass('switcher-big-hide');
        }
    }, 200)
})

if ($(window).width() > 1279) {
    if ($('.banner.active').length > 0) {
        $('.nav-switcher').hide();

    } else if ($('.banner.active').length < 0) {
        $('.nav-switcher').show();
    }
}


$('.banner-choices .choice').on('click', function(){
    $('.banner-choices .choice').removeClass('active');
    $(this).addClass('active');
})

$('.how .active-number').text($('.how .card.active').index() + 1);

if ($('.how .card1.active').length > 0) {
    $('.how .arrow-left').hide();
}

$('.how .arrow-right').on('click', function () {
    $('.how .card.active').next().addClass('active');
    $('.how .card.active').prev().removeClass('active');
})

$('.how .arrow-left').on('click', function () {
    $('.how .card.active').prev().addClass('active');
    $('.how .card.active').next().removeClass('active');
})

$('.how .arrow').on('click', function () {
    if ($('.how .card1.active').length > 0) {
        $('.how .arrow-left').fadeOut();
        $('.how .arrow-right').fadeIn();
    } else if ($('.how .card3.active').length > 0) {
        $('.how .arrow-right').fadeOut();
        $('.how .arrow-left').fadeIn();
    } else if ($('.how .card2.active').length > 0) {
        $('.how .arrow-left').fadeIn();
        $('.how .arrow-right').fadeIn();
    }
    $('.how .active-number').text($('.how .card.active').index() + 1);
    $('.how h2').hide();
    let title = $('.how .card.active').attr('data-id');
    $('#' + title).show();
})

$('.how .card').on('click', function () {
    $('.how .card').removeClass('active');
    $(this).addClass('active');
    $('.how h2').hide();
    let title = $(this).attr('data-id');
    $('#' + title).show();
    $('.how .active-number').text($(this).index() + 1);
    if ($('.how .card1.active').length > 0) {
        $('.how .arrow-left').fadeOut();
        $('.how .arrow-right').fadeIn();
    } else if ($('.how .card3.active').length > 0) {
        $('.how .arrow-right').fadeOut();
        $('.how .arrow-left').fadeIn();
    } else if ($('.how .card2.active').length > 0) {
        $('.how .arrow-left').fadeIn();
        $('.how .arrow-right').fadeIn();
    }
});

$('.flying-numbers .active-number').text($('.how .card.active').index() + 1);

if ($('.flying-numbers .card1.active').length > 0) {
    $('.flying-numbers .arrow-left').hide();
}

$('.flying-numbers .arrow-right').on('click', function () {
    $('.flying-numbers .card.active').next().addClass('active');
    $('.flying-numbers .card.active').prev().removeClass('active');
})

$('.flying-numbers .arrow-left').on('click', function () {
    $('.flying-numbers .card.active').prev().addClass('active');
    $('.flying-numbers .card.active').next().removeClass('active');
})

$('.flying-numbers .arrow').on('click', function () {
    if ($('.flying-numbers .card1.active').length > 0) {
        $('.flying-numbers .arrow-left').fadeOut();
        $('.flying-numbers .arrow-right').fadeIn();
    } else if ($('.flying-numbers .card3.active').length > 0) {
        $('.flying-numbers .arrow-right').fadeOut();
        $('.flying-numbers .arrow-left').fadeIn();
    } else if ($('.flying-numbers .card2.active').length > 0) {
        $('.flying-numbers .arrow-left').fadeIn();
        $('.flying-numbers .arrow-right').fadeIn();
    }
    $('.flying-numbers .active-number').text($('.flying-numbers .card.active').index() + 1);
    $('.flying-numbers h2').hide();
    let title = $('.flying-numbers .card.active').attr('data-id');
    $('#' + title).show();
})

$('.flying-numbers .card').on('click', function () {
    $('.flying-numbers .card').removeClass('active');
    $(this).addClass('active');
    $('.flying-numbers h2').hide();
    let title = $(this).attr('data-id');
    $('#' + title).show();
    $('.flying-numbers .active-number').text($(this).index() + 1);
    if ($('.flying-numbers .card1.active').length > 0) {
        $('.flying-numbers .arrow-left').fadeOut();
        $('.flying-numbers .arrow-right').fadeIn();
    } else if ($('.flying-numbers .card3.active').length > 0) {
        $('.flying-numbers .arrow-right').fadeOut();
        $('.flying-numbers .arrow-left').fadeIn();
    } else if ($('.flying-numbers .card2.active').length > 0) {
        $('.flying-numbers .arrow-left').fadeIn();
        $('.flying-numbers .arrow-right').fadeIn();
    }
});

$('.flying-cheap .card-wrap').on('click', function () {
    $('.flying-cheap .card-wrap').removeClass('active');
    $(this).addClass('active');
});

$('.availables .fly').mouseover(function () {
    $('.availables .fly').removeClass('active');
    $(this).addClass('active');
});

$('.availables .fly').mouseout(function () {
    $(this).removeClass('active');
    $('.availables .fly').first().addClass('active');
});

$('.availables .directions .map').mouseover(function () {
    $('.availables .directions .map').removeClass('active');
    $(this).addClass('active');
});

$('.availables .directions .map').mouseout(function () {
    $(this).removeClass('active');
    $('.availables .directions .map').first().addClass('active');
});

$('.questions .item').on('click', function () {
    $(this).toggleClass('active');
})

$('.discover-experiences .small-items .item').on('click', function(){
    $('.discover-experiences .small-items .item').removeClass('violet');
    $(this).addClass('violet');
    $('.discover-experiences .discover-item-big').hide();
    let itemID = $(this).attr('data-id');
    $('#' + itemID).show()
})

let currentDataId = $('.review-slider .slick-current').attr('data-id');
$('.reviews .review-info').hide();
$('#' + currentDataId).show();
$('.reviews .all-items').text($('.review-slider .slick-slide').length);
$('.review-slider').on('afterChange', function () {
    let currentDataId = $('.review-slider .slick-current').attr('data-id');
    $('.reviews .review-info').hide();
    $('#' + currentDataId).show();
    $('.reviews .active-number').text($('.review-slider .slick-current').index() + 1);
});

if ($(window).width() < 1280) {
    $('.nav-buttons').addClass('mobile-nav');
}


if ($(window).width() < 1280) {
    $('.show-mobile').on('click', function () {
        $('.nav-buttons').show();
        $(this).hide();
        $('.close-mobile').show();
    });

    $('.close-mobile').on('click', function () {
        $('.nav-buttons').hide();
        $(this).hide();
        $('.show-mobile').show();
    })
}

